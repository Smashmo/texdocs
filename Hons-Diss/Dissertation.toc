\contentsline {section}{\numberline {1}Introduction}{9}{section.1}
\contentsline {subsection}{\numberline {1.1}Defining Terms}{9}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Background}{11}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Aims \& Objectives}{13}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Scope of this Investigation}{13}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Constraints of the Investigation}{14}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Chapter Outlines}{14}{subsection.1.6}
\contentsline {section}{\numberline {2}Forensic and Technical Literature}{16}{section.2}
\contentsline {subsection}{\numberline {2.1}Current Forensics Methodology}{16}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Digital Forensics in Traditional Data Storage Pools}{19}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}NAS \& SAN}{22}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}NFS \& Samba}{24}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}RAID}{25}{subsubsection.2.2.3}
\contentsline {subsection}{\numberline {2.3}Current Digital Forensics Programs}{29}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Forensics Challenges of Distributed and Parallel Filings systems}{30}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Gluster}{30}{subsubsection.2.4.1}
\contentsline {subsubsection}{\numberline {2.4.2}HDFS}{32}{subsubsection.2.4.2}
\contentsline {subsection}{\numberline {2.5}Exploring Ceph}{34}{subsection.2.5}
\contentsline {subsection}{\numberline {2.6}Exploring XFS}{40}{subsection.2.6}
\contentsline {subsection}{\numberline {2.7}Literature Conclusion}{47}{subsection.2.7}
\contentsline {section}{\numberline {3}Preparations and Design}{48}{section.3}
\contentsline {subsection}{\numberline {3.1}Building a Ceph cluster with KVM/Libvert}{48}{subsection.3.1}
\contentsline {section}{\numberline {4}Evaluation Framework}{52}{section.4}
\contentsline {section}{\numberline {5}Implementing Forensics on Ceph}{54}{section.5}
\contentsline {subsection}{\numberline {5.1}XFS Forensics}{56}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Performing forensics on an OSD}{64}{subsubsection.5.1.1}
\contentsline {subsection}{\numberline {5.2}Ceph Forensics}{66}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}RADOS \& RBD Infrastructure}{67}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Cluster Maps}{71}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Finalising Ceph Forensics}{76}{subsubsection.5.2.3}
\contentsline {section}{\numberline {6}Findings of Evaluation \& Discussion of Results}{77}{section.6}
\contentsline {section}{\numberline {7}Conclusions}{82}{section.7}
\contentsline {section}{\numberline {8}Recommendations}{84}{section.8}
\contentsline {section}{Appendices}{88}{section*.46}
\contentsline {section}{\numberline {A}Initial Project Overview}{88}{Appendix.a.A}
\contentsline {section}{\numberline {B}Second Formal Review Output}{92}{Appendix.a.B}
\contentsline {section}{\numberline {C}Diary Sheets (or other project management evidence)}{95}{Appendix.a.C}
\contentsline {section}{\numberline {D}Appendix 4}{118}{Appendix.a.D}
